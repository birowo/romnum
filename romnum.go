package main

const maxdigits = 20

func romawi(num int) string {
	romans := [3][10]string{
		{"", "I", "II", "III", "VI", "V", "IV", "IIV", "IIIV", "XI"},
		{"", "X", "XX", "XXX", "LX", "L", "XL", "XXL", "XXXL", "CX"},
		{"", "C", "CC", "CCC", "DC", "D", "CD", "CCD", "CCCD", "MC"},
	}
	i := 0
	j := 0
	var rslt [maxdigits]byte
	for num > 0 && i < 3 {
		j += copy(rslt[j:], romans[i][num%10])
		i++
		num = num / 10
	}
	num += j
	if num >= maxdigits {
		return ""
	}
	for j < num {
		rslt[j] = 'M'
		j++
	}
	k := j
	i = 0
	for i < j { //reverse
		j--
		rslt[i], rslt[j] = rslt[j], rslt[i]
		i++
	}
	return string(rslt[:k])
}
func main() {
	println(romawi(3999))
}
